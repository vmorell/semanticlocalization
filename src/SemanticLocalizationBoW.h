 /*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#ifndef _SEMANTICLOCALIZATIONBOW_H_
#define _SEMANTICLOCALIZATIONBOW_H_

#include "SemanticLocalization.h"

#include <iostream>
#include <sstream>
#include <vector>

#include <pcl/ml/kmeans.h>

namespace fs = boost::filesystem;
using namespace std;

class SemanticLocalizationBoW : public  SemanticLocalization
{
public:
	SemanticLocalizationBoW();	

	virtual void train() = 0;
	virtual int classifyFrameScene(std::vector<float*> &features) = 0;

	void computeDictionary(std::vector<float*> &featuresInput, std::vector<float*> &dictionary);
	float* wordsAssignation(std::vector<float*>  &featuresFromFile);

	virtual ~SemanticLocalizationBoW();

	int maxTrainingFeatures;

	std::vector<float*> dictionary;		
	int dictionarySize;

	void generateTrainingWords(bool saveCSV);
	void trainingWordAssignation();
	void generateDictionaryFromTraining();

	std::vector<float*> trainingWords;
	std::vector<int> trainingClasses;

private:
};

#endif
