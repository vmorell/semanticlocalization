/*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */
#include "SemanticFeatureExtractorNARF.h"
#include "FeatureExtractionCommon.h"

#include <pcl/range_image/range_image_planar.h>
#include <pcl/features/range_image_border_extractor.h>
#include <pcl/keypoints/narf_keypoint.h>
#include <pcl/features/narf_descriptor.h>


SemanticFeatureExtractorNARF::SemanticFeatureExtractorNARF()
{
	name = "NARF";
	dimensionality = pcl::Narf36::descriptorSize();
}

void SemanticFeatureExtractorNARF::loadFeatures(std::string file, std::vector<float*> &features)
{
	//load the pointcloud of features
	PointCloud<pcl::Narf36>::Ptr f_src(new PointCloud<pcl::Narf36>);
	pcl::io::loadPCDFile(file, *f_src);
	//reserve space for the features
	features.clear();
	features.reserve(f_src->size());
	//convert to vector<float*>
	for (int i = 0; i<f_src->size(); i++){
		float *desc = new float[dimensionality];
		for (int j = 0; j < dimensionality; j++)
		{
			desc[j] = f_src->points[i].descriptor[j];
		}
		features.push_back(desc);


	}
}
void SemanticFeatureExtractorNARF::saveFeatures(std::string file, std::vector<float*> features)
{
	PointCloud<pcl::Narf36>::Ptr f_src(new PointCloud<pcl::Narf36>);
	//reserve space for the features
	f_src->resize(features.size());
	//convert to pointcloud of pcl::feature
	for (int i = 0; i<features.size(); i++){
		for (int j = 0; j < dimensionality; j++)
		{
			f_src->points[i].descriptor[j] = features.at(i)[j];
		}
	}
	pcl::io::savePCDFile(file, *f_src, true);
}


void SemanticFeatureExtractorNARF::extractFeatures(const PointCloud<PointXYZRGB>::Ptr &src,
	const PointCloud<PointXYZRGB>::Ptr &keypoints,
	std::vector<float*> &features)
{

	PointCloud<pcl::Narf36>::Ptr f_src(new PointCloud<pcl::Narf36>);


	// Convert the cloud to range image.
	int imageSizeX = 640, imageSizeY = 480;
	float centerX = (640.0f / 2.0f), centerY = (480.0f / 2.0f);
	float focalLengthX = 525.0f, focalLengthY = focalLengthX;
	Eigen::Affine3f sensorPose = Eigen::Affine3f(Eigen::Translation3f(src->sensor_origin_[0],
		src->sensor_origin_[1],
		src->sensor_origin_[2])) *
		Eigen::Affine3f(src->sensor_orientation_);
	float noiseLevel = 0.0f, minimumRange = 0.0f;
	pcl::RangeImagePlanar rangeImage;
	rangeImage.createFromPointCloudWithFixedSize(*src, imageSizeX, imageSizeY,
		centerX, centerY, focalLengthX, focalLengthX,
		sensorPose, pcl::RangeImage::CAMERA_FRAME,
		noiseLevel, minimumRange);

	// The NARF estimator needs the indices in a vector, not a cloud.
	std::vector<int> keypoints2;
	keypoints2.resize(keypoints->points.size());
	
	int nres;
	std::vector<int> indices(2);
	std::vector<float> sqr_distances(2);
	pcl::search::KdTree<PointXYZRGB> tree;
	tree.setInputCloud(src);

	for (unsigned int i = 0; i < keypoints->size(); ++i)
	{
		//Considering the second neighbor since the first is the point itself.
		nres = tree.nearestKSearch(i, 2, indices, sqr_distances);
		if (nres == 2)
		{
			keypoints2[i] = indices[1];
		}
	}

	// NARF estimation object.
	pcl::NarfDescriptor narf(&rangeImage, &keypoints2);
	// Support size: choose the same value you used for keypoint extraction.
	narf.getParameters().support_size = 0.2f;
	// If true, the rotation invariant version of NARF will be used. The histogram
	// will be shifted according to the dominant orientation to provide robustness to
	// rotations around the normal.
	narf.getParameters().rotation_invariant = true;

	narf.compute(*f_src);

	features.reserve(f_src->size());
	//convert to vector<float*>
	for (int i = 0; i<f_src->size(); i++){
		float v = f_src->points[i].descriptor[0];
		if (!pcl_isfinite(v)){
			f_src->points.erase(f_src->points.begin() + i);
			i--;
		}
		else{
			float *desc = new float[dimensionality];
			for (int j = 0; j < dimensionality; j++)
			{
				desc[j] = f_src->points[i].descriptor[j];
			}
			features.push_back(desc);


		}

	}
}


SemanticFeatureExtractorNARF::~SemanticFeatureExtractorNARF()
{
}
