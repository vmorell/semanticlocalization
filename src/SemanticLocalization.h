 /*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#ifndef _SEMANTICLOCALIZATION_H_
#define _SEMANTICLOCALIZATION_H_

#include <iostream>
#include <sstream>
#include <vector>

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/filesystem.hpp>

#include <pcl/features/feature.h>
#include <pcl/io/pcd_io.h>

#include "SemanticFeatureExtractor.h"
#include "SemanticKeypointDetector.h"

namespace fs = boost::filesystem;
using namespace std;

/*
*	Class for the management of the frames used to train/evaluate semantic localization systems
*
*/

class FrameItem{
public:
	//stores the name of the classes
	static std::vector<std::string> classes;

	//stores the name of the objects
	static std::vector<std::string> object_names;

	std::string color_path;
	std::string depth_path;

	//class of the item
	int item_class;
	boost::shared_ptr<std::vector<int> > object_appearances;

	FrameItem(){};

	FrameItem(string colorpath, string depthpath,int it_class)
	{
		color_path = colorpath;
		depth_path = depthpath;
		item_class = it_class;
	};
};


class SemanticLocalization{
public:

	int numClases;

	SemanticLocalization();
	
	void readConfigurationTraining(fs::path conf_path);
	void readConfigurationTest(fs::path conf_path);

	double test();		
	void validate();

	virtual void train() = 0; 		
	virtual int classifyFrameScene(std::vector<float*> &features) = 0;
	virtual ~SemanticLocalization();

	fs::path dataset_path;

	std::vector<FrameItem> framesTraining;
	std::vector<FrameItem> framesTest;
	
	SemanticKeypointDetector * detector;
	SemanticFeatureExtractor * extractor;

	void saveCSVFile(std::vector<float*> &featuresToSave, std::vector<int> &classesToSave, string fileName, int featuresLength);	     
	void loadCSVFile(std::vector<float*> &featuresToLoad, std::vector<int> &classesToLoad, string fileName);

	private:
};

#endif
