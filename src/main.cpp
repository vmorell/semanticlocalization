 /*
  * Software License Agreement (BSD License)
  *
  *  Semantic Localization in the PCL Library
  *  Copyright (c) 2015. Jesus Martinez-Gomez
  *
  *  All rights reserved.
  *
  *  Redistribution and use in source and binary forms, with or without
  *  modification, are permitted provided that the following conditions
  *  are met:
  *
  *   * Redistributions of source code must retain the above copyright
  *     notice, this list of conditions and the following disclaimer.
  *   * Redistributions in binary form must reproduce the above
  *     copyright notice, this list of conditions and the following
  *     disclaimer in the documentation and/or other materials provided
  *     with the distribution.
  *   * Neither the name of Willow Garage, Inc. nor the names of its
  *     contributors may be used to endorse or promote products derived
  *     from this software without specific prior written permission.
  *
  *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
  *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
  *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
  *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
  *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
  *  POSSIBILITY OF SUCH DAMAGE.
  *
  */

#include "SemanticKeypointDetectorUniform.h"
#include "SemanticKeypointDetectorHarris3D.h"
#include "SemanticKeypointDetectorNARF.h"

#include "SemanticFeatureExtractorSHOT.h"
#include "SemanticFeatureExtractorColorSHOT.h"
#include "SemanticFeatureExtractorFPFH.h"
#include "SemanticFeatureExtractorPFHRGB.h"
#include "SemanticFeatureExtractorNARF.h"

#include "SemanticLocalization.h"
#include "SemanticLocalizationBoWSVM.h"
#include "SemanticLocalizationBoWKnn.h"

static void show_usage(std::string name)
{
    std::cerr << "Usage: " << name << " <option(s)> \n"
              << "Options:\n"
              << "\t-c\tCLASSIFICATION MODEL\tSpecify the classifier type: 0(SVM) or 1(kNN) \n"
              << "\t\n"
              << "\t-kp\tKEYPOINT DETECTOR\tSpecify the keypoint detector type: 0(Harris3D), 1(Narf) or 2(Uniform) \n"
              << "\t-ft\tFEATURES EXTRACTOR\tSpecify the features extractor type: 0(ColorShot), 1(FPFH), 2(NARF), 3(PFHRGB) or 4(SHOT) \n"
              << "\t-dict\tDICTIONARY SIZE\tSpecify the dictionary size: integer value \n"
              << "\t-trainConfFile\tTRAINING CONFIGURATION FILE\tSpecify the name of the training configuration file.\n"
              << "\t-testConfFile\tTEST CONFIGURATION FILE\tSpecify the name of the test configuration file.\n"
              << "\t\n"
              << "\t-svmType\tSVM KERNEL\tSpecify the kernel used in the SVM: 0(LINEAR), 1(POLY), 2(RBF), 3(SIGMOID) or 4(Exp.chi²) (Only with -c 0 option) \n"
              << "\t-kValue\tK VALUE\tSpecify the k parameter for the kNN classifier (Only with -c 1 option) \n"
              << std::endl;
}

int main (int argc, char *argv[])
{
  SemanticLocalization* semanticLocalizationSystem; 
  int classificationType = -1;

  if (argc < 15) 
	{
        show_usage(argv[0]);
        return 1;
	}
  
   std::string arg = argv[1];
   if (arg == "-c")
	{
	std::string arg2 = argv[2];
	int classifierType = boost::lexical_cast<int>(arg2);
	switch (classifierType)
		{
		case 0:
			semanticLocalizationSystem = new SemanticLocalizationBoWSVM();
			semanticLocalizationSystem->dataset_path = "../data/";
			classificationType = 0;
			cout << "SVM CLASSIFIER"<< endl;
			break;
		case 1:
			semanticLocalizationSystem = new SemanticLocalizationBoWKnn();  			
			semanticLocalizationSystem->dataset_path = "../data/";
			cout << "kNN CLASSIFIER"<< endl;
			classificationType = 1;
			break;
		default:
			std::cerr << "Wrong classifier type." << std::endl;
		    	show_usage(argv[0]);
		        return 1;
		}				
	}	 		
   else 
	{ 
	  std::cerr << "First option must be the classification type." << std::endl;
	  return 1;
	}  

   for (int i = 3; i < argc; ++i) 
	{
	std::string arg = argv[i];
        if (arg == "-kp")
		{
            	if (i + 1 < argc) 	
			{
			i = i+1;
			std::string arg2 = argv[i];
			int keypointType = boost::lexical_cast<int>(arg2);
			switch (keypointType)
				{
				case 0:
					semanticLocalizationSystem->detector = new SemanticKeypointDetectorHarris3D(); 
					cout << "HARRIS3D KEYPOINT DETECTOR"<< endl;
					break;
				case 1:
					semanticLocalizationSystem->detector = new SemanticKeypointDetectorNARF();  			
					cout << "NARF KEYPOINT DETECTOR"<< endl;
					break;
				case 2:
					semanticLocalizationSystem->detector = new SemanticKeypointDetectorUniform();  			
					cout << "UNIFORM KEYPOINT DETECTOR"<< endl;
					break;
				default:
					std::cerr << "Wrong keypoint detector type." << std::endl;
			            	show_usage(argv[0]);
			                return 1;
				}				
			}	 		
		else 
		{ 
                  std::cerr << "Keypoint detector option requires one argument." << std::endl;
                  return 1;
            	}  
        }
	else if (arg == "-ft")
		{
            	if (i + 1 < argc) 	
			{
			i = i+1;
			std::string arg2 = argv[i];
			int featureType = boost::lexical_cast<int>(arg2);
			switch (featureType)
				{
				case 0:
					semanticLocalizationSystem->extractor = new SemanticFeatureExtractorColorSHOT(); 
					cout << "COLORSHOT FEATURES EXTRACTOR"<< endl;
					break;				
				case 1:
					semanticLocalizationSystem->extractor = new SemanticFeatureExtractorFPFH();  			
					cout << "FPFH FEATURES EXTRACTOR"<< endl;
					break;
				case 2:
					semanticLocalizationSystem->extractor = new SemanticFeatureExtractorNARF();  	
					cout << "NARF FEATURES EXTRACTOR"<< endl;		
					break;
				case 3:
					semanticLocalizationSystem->extractor = new SemanticFeatureExtractorPFHRGB();  	
					cout << "PFHRGB FEATURES EXTRACTOR"<< endl;		
					break;
				case 4:
					semanticLocalizationSystem->extractor = new SemanticFeatureExtractorSHOT();  	
					cout << "SHOT FEATURES EXTRACTOR"<< endl;		
					break;
				default:
					std::cerr << "Wrong feature extractor type." << std::endl;
			            	show_usage(argv[0]);
			                return 1;
				}				
			}	 		
		else 
		{ 
                  std::cerr << "Features extractor option requires one argument." << std::endl;
                  return 1;
            	}  
	}
	else if (arg == "-dict")
		{
            	if (i + 1 < argc) 	
			{
			i = i+1;
			std::string arg2 = argv[i];
			int dictSize = boost::lexical_cast<int>(arg2);
		  	((SemanticLocalizationBoW*)semanticLocalizationSystem)->dictionarySize = dictSize;	
			cout << "DICTIONARY SIZE:" << dictSize << endl;						
			}	 		
		else 
		{ 
                  std::cerr << "Dictionary Size requires one argument." << std::endl;
                  return 1;
            	}  
        }
	else if (arg == "-trainConfFile")
		{
            	if (i + 1 < argc) 	
			{
			i = i+1;
			std::string arg2 = argv[i];		
			semanticLocalizationSystem->readConfigurationTraining(arg2);
			cout << "TRAINING CONFIGURATION FILE:" << arg2 << endl;
			}	 		
		else 
		{ 
                  std::cerr << "Training Configuration File requires one argument." << std::endl;
                  return 1;
            	}  
        }
	else if (arg == "-testConfFile")
		{
            	if (i + 1 < argc) 	
			{
			i = i+1;
			std::string arg2 = argv[i];		
			semanticLocalizationSystem->readConfigurationTest(arg2);
			cout << "TEST CONFIGURATION FILE:" << arg2 << endl;
			}	 		
		else 
		{ 
                  std::cerr << "Test Configuration File requires one argument." << std::endl;
                  return 1;
            	}  
        }
	else if (arg == "-svmType")
		{
		if(classificationType==0)		
		{		
		    	if (i + 1 < argc) 	
				{
				i = i+1;
				std::string arg2 = argv[i];
				int svmType = boost::lexical_cast<int>(arg2);
				switch (svmType)
					{
					case 0:
						((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = LINEAR;
						cout << "LINEAR SVM" << endl;
						break;				
					case 1:
						((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = POLY;  			
						cout << "POLY SVM" << endl;
						break;
					case 2:
						((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = RBF;  			
						cout << "RBF SVM" << endl;
						break;
					case 3:
						((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = SIGMOID;  	
						cout << "SIGMOID SVM" << endl;		
						break;
					case 4:
						((SemanticLocalizationBoWSVM*)semanticLocalizationSystem)->svmParameters.kernel_type = PRECOMPUTED;  			
						cout << "PRECOMPUTED CHI-2 SVM" << endl;
						break;
					default:
						std::cerr << "Wrong svmType value." << std::endl;
					    	show_usage(argv[0]);
						return 1;
					}				
				}	 		
			else 
			{ 
		          std::cerr << "svmType requires one argument." << std::endl;
		          return 1;
		    	}
		}
		else
		{
			std::cerr << "svmType option only with SVM classifiers (-c 0)" << std::endl;
		        return 1;
		}  
        }
	else if (arg == "-kValue")
		{
		if(classificationType==1)		
		{		
		    	if (i + 1 < argc) 	
				{
				i = i+1;
				std::string arg2 = argv[i];
				int kValue = boost::lexical_cast<int>(arg2);
			  	((SemanticLocalizationBoWKnn*)semanticLocalizationSystem)->kValue = kValue;	
				cout << kValue << "NN CLASSIFIER" << endl;
			
				}	 		
			else 
			{ 
		          std::cerr << "kValue requires one argument." << std::endl;
		          return 1;
		    	}
		}
		else
		{
			std::cerr << "kValue option only with kNN classifiers (-c 1)" << std::endl;
		        return 1;
		}  
        }
	else 
		{
                std::cerr << "Wrong options." << std::endl;
            	show_usage(argv[0]);
	        return 0;
        	}
	}


  semanticLocalizationSystem->train();  
  semanticLocalizationSystem->test();
}
