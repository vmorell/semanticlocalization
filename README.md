### Overview ###

* PCL implementation of the Bag of Words approach for semantic localization
* 1.1

### How do I get set up? ###

* First Steps 

You should clone the repository to have in your desired PATH two folders: /src and /data

* Compilation

You can compile in two different ways

1.- For its use with a Graphical User Interface.- It requires QT and VTK properly installed

$/mkdir build
$/cd build
$/cmake -D QT_GUI=TRUE ../src
$/make

2.- For its use with a command line

$/mkdir build
$/cd build
$/cmake ../src
$/make

* Dependencies

PCL 1.8  (Boost, Flann, ..)
QT and VTK for the GUI compilation

* Database configuration

All data for a basic compilation is in folder /data/. For additional tests, download .pcd files from http://www.rovit.ua.es/dataset/vidrilo/ and use the appropriate configuration files configurationSequence1Vidrilo.txt or configurationSequence2Vidrilo.txt (both in /data/)

* How to run tests

$/ ./semanticClassification <option(s)> 
Options:
	-c	CLASSIFICATION MODEL	Specify the classifier type: 0(SVM) or 1(kNN) 
	
	-kp	KEYPOINT DETECTOR	Specify the keypoint detector type: 0(Harris3D), 1(Narf) or 2(Uniform) 
	-ft	FEATURES EXTRACTOR	Specify the features extractor type: 0(ColorShot), 1(FPFH), 2(NARF), 3(PFHRGB) or 4(SHOT) 
	-dict	DICTIONARY SIZE	Specify the dictionary size: integer value 
	-trainConfFile	TRAINING CONFIGURATION FILE	Specify the name of the training configuration file.
	-testConfFile	TEST CONFIGURATION FILE	Specify the name of the test configuration file.
	
	-svmType	SVM KERNEL	Specify the kernel used in the SVM: 0(LINEAR), 1(POLY), 2(RBF), 3(SIGMOID) or 4(Exp.chi²) (Only with -c 0 option) 
	-kValue	K VALUE	Specify the k parameter for the kNN classifier (Only with -c 1 option) 

Examples of use: 

	./semanticClassification -c 0 -kp 1 -ft 4 -dict 20 -trainConfFile ../data/trainConfmini.txt -testConfFile ../data/testConfmini.txt -svmType 4

	--> SVM CLASSIFIER
	--> NARF KEYPOINT DETECTOR
	--> SHOT FEATURES EXTRACTOR
	--> DICTIONARY SIZE:20
	--> TRAINING CONFIGURATION FILE:trainConfmini.txt
	--> TEST CONFIGURATION FILE:testConfmini.txt
	--> PRECOMPUTED CHI-2 SVM

	./semanticClassification -c 1 -kp 2 -ft 1 -dict 5 -trainConfFile ../data/trainConfmini.txt -testConfFile ../data/testConfmini.txt -kValue 7

	--> kNN CLASSIFIER
	--> UNIFORM KEYPOINT DETECTOR
	--> FPFH FEATURES EXTRACTOR
	--> DICTIONARY SIZE:5
	--> TRAINING CONFIGURATION FILE:trainConfmini.txt
	--> TEST CONFIGURATION FILE:testConfmini.txt
	--> 7NN CLASSIFIER